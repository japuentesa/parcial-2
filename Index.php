<?php




?>
<!doctype html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" />
    <link href="https://bootswatch.com/4/superhero/bootstrap.css" rel="stylesheet" />
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">

    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>


    <title>ICAI-UD</title>
    <link rel="icon" type="image/png" href="icai.png">
    <script type="text/javascript">
        $(function() {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
</head>

<body>


    <br>
    <div class="container" style="background-color: #9c9c9c;">
        <div class="row">
            <div class="col-lg-3 col-md-4 text-center">
                <img src="icai.png" width="200">
            </div>
            <div class="row pt-4 ">
                <div class="col-lg-8 col-md-9">
                    <h4>
                        <i><strong>Fourth International Conference on Applied Informatics</strong></i>
                    </h4>
                    <h5>
                        28 to 30 October 2021 <br> Buenos Aires, Argentina
                    </h5>
                </div>
            </div>
        </div>
        <br>

        <br>
        <div class="row mt-3">
            <div class="col-lg-4"></div>
            <div class="col-lg-4 text-center">
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <label class="input-group-text">Edition</label>
                        </div>
                        <select class="custom-select" id="edition">
                            <option value="-1">Select edition</option>
                            <option value="3">2020</option>
                            <option value="2">2019</option>
                            <option value="1">2018</option>
                        </select>

                    </div>
                </div>
            </div>
        </div>

        <div id="result">
        
        </div>


        <script>
            $(document).ready(function() {
                $("#edition").change(function() {
                    if ($("#edition").val() != -1) {
                        $("#result").html("<div class='text-center'><img src='Loadingg.gif'></div>");
                        var edition = $("#edition").val();
                        var path = "torta.php?&e="+edition;
                        $("#result").load(path);
                    } else {
                        $("#result").html("");
                    }
                });
            });
        </script>
        <hr>

        <hr>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <div class="card bg-light">
                    <div class="card-header">Visitors</div>
                    <div class="card-body">
                        <script type="text/javascript" id="clustrmaps" src="//cdn.clustrmaps.com/map_v2.js?d=p1GQ85WJS_ieJFW7M6EtdMiVy9rV7Gz2ZMyKu44nWFw&cl=ffffff&w=a"></script>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-center text-muted">
            &copy; ITI Research Group<br>2018 - 2021 All rights reserved
        </div>
    </div>
    <br>


</body>

</html>