<?php
require "EdicionTopicDAO.php";

class EdicionTopic{
    private $idEdicionTopic;
    private $acepted;
    private $rejected;
    private $val;
    
    public function getIdEdicionTopic()
    {
        return $this->idEdicionTopic;
    }

    
    public function getAcepted()
    {
        return $this->acepted;
    }

   
    public function getRejected()
    {
        return $this->rejected;
    }
    

    function EdicionTopic ($pIdEdicionTopic="", $pAcepted="", $pRejected="", $pVal="") {
        $this -> idEdicionTopic = $pIdEdicionTopic;
        $this -> acepted = $pAcepted;
        $this -> rejected = $pRejected;
        $this -> val = $pVal;
        $this -> conexion = new Conexion();
        $this -> EdicionTopicDAO = new EdicionTopicDAO($pIdEdicionTopic, $pAcepted, $pRejected,$pVal);        
    }
       
    function consultarporEdicion(){
        $this -> conexion -> abrir();
        echo $this -> EdicionTopicDAO -> consultarporEdicion();
        $this -> conexion -> ejecutar($this -> EdicionTopicDAO -> consultarporEdicion());
        $this -> conexion -> cerrar();
        $resultados = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($resultados, array($resultado[0],$resultado[1]));
        }
        return $resultados;
    }
    
}


?>