<?php
require "EdicionDAO.php";

class Edicion{
    private $idEdicion;
    private $name;
    private $year;
    
    public function getIdEdicion()
    {
        return $this->idEdicion;
    }

    
    public function getName()
    {
        return $this->name;
    }

   
    public function getYear()
    {
        return $this->year;
    }
    

    function Edicion ($pIdEdicion="", $pname="", $pyear="") {
        $this -> idEdicion = $pIdEdicion;
        $this -> name = $pname;
        $this -> year = $pyear;
        $this -> conexion = new Conexion();
        $this -> EdicionDAO = new EdicionDAO($pIdEdicion, $pname, $pyear);        
    }
       
    
}


?>